﻿using HubertDumas.DAL.Blogs;
using HubertDumas.DAL.IdentityServer;
using Microsoft.EntityFrameworkCore;

namespace HubertDumas.DAL
{
    public class SweetSourContext : DbContext
    {
        public SweetSourContext(DbContextOptions<SweetSourContext> options) : base(options) { }

        public DbSet<HubertDumas.DAL.User.User> Users { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<BlogMedia> BlogMedias { get; set; }
        public DbSet<HubertDumas.DAL.Media.Media> Medias { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<AuthorizationServer> AuthorizationServers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<HubertDumas.DAL.User.User>().ToTable("Users");

            builder.Entity<Blog>(e =>
            {
                e.ToTable("Blogs")
                    .HasKey(c => c.Id);

                e.Property(c => c.Id)
                    .ValueGeneratedOnAdd();

                e.Property(c => c.Title)
                    .IsRequired()
                    .HasMaxLength(50);

                e.Property(c => c.Subtitle)
                    .IsRequired()
                    .HasMaxLength(300);

                e.Property(c => c.MediaId);

                e.HasOne(c => c.Media)
                    .WithMany(d => d.Blogs)
                    .HasForeignKey(f => f.MediaId);
            });

            builder.Entity<BlogMedia>(e =>
            {
                e.ToTable("BlogMedias")
                    .HasKey(c => c.Id);

                e.Property(c => c.Id)
                    .ValueGeneratedOnAdd();

                e.HasOne(c => c.Blog)
                    .WithMany(m => m.BlogMedias)
                    .HasForeignKey(k => k.BlogId);

                e.Property(c => c.BlogUtcDate)
                    .IsRequired();

                e.Property(c => c.MediaId)
                    .IsRequired();

                e.Property(c => c.Caption)
                    .HasColumnName("Caption")
                    .IsRequired()
                    .HasMaxLength(50);

                e.Property(c => c.Description)
                    .IsRequired()
                    .HasColumnName("Description")
                    .HasMaxLength(300);

                e.Property(c => c.LongDescription)
                    .HasMaxLength(600);

                e.HasOne(c => c.Media)
                    .WithMany(d => d.BlogMedias)
                    .HasForeignKey(f => f.MediaId);

            });

            builder.Entity<HubertDumas.DAL.Media.Media>(e =>
            {
                e.ToTable("Medias")
                    .HasKey(c => c.Id);

                e.Property(c => c.Id)
                    .ValueGeneratedOnAdd();

                e.Property(c => c.Source)
                    .IsRequired()
                    .HasMaxLength(300);
            });

            builder.Entity<AuthorizationServer>(e =>
            {
                e.ToTable("AuthorizationServers")
                    .HasKey(c => c.Id);

                e.Property(c => c.Id)
                    .IsRequired();

                e.Property(c => c.Url)
                    .IsRequired();
            });

            builder.Entity<Client>(e =>
            {
                e.ToTable("Clients")
                    .HasKey(c => c.Id);

                e.HasOne(c => c.AuthorizationServer)
                    .WithMany(m => m.Clients)
                    .HasForeignKey(k => k.AuthorizationServerId);
            });

               
        }
    }
}
