﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HubertDumas.DAL.User
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByCredentials(string username, string password);
    }
}
