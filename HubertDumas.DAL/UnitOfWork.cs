﻿using HubertDumas.DAL.Blogs;
using HubertDumas.DAL.IdentityServer;
using HubertDumas.DAL.Media;
using HubertDumas.DAL.User;

namespace HubertDumas.DAL
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly SweetSourContext _context;

        public UnitOfWork(SweetSourContext context)
        {
            _context = context;

            Clients = new ClientRepository(_context);

            Users = new UserRepository(_context);
            Medias = new MediaRepository(_context);
            Blogs = new BlogRepository(_context);
            BlogMedias = new BlogMediaRepository(_context);
        }

        public IClientRepository Clients { get; }
        public IUserRepository Users { get; }
        public IMediaRepository Medias { get; }
        public IBlogRepository Blogs { get; }
        public IBlogMediaRepository BlogMedias { get; }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
