﻿using System.Collections.Generic;
using System.Linq;
using HubertDumas.DAL;

namespace HubertDumas.DAL.Media
{
    public class MediaRepository : Repository<Media>, IMediaRepository
    {
        private readonly SweetSourContext _entities;

        public MediaRepository(SweetSourContext context) : base(context)
        {
            _entities = context;
        }
    }
}
