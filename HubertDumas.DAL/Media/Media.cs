﻿using HubertDumas.DAL.Blogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace HubertDumas.DAL.Media
{
    public class Media
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Source { get; set; }

        public List<Blog> Blogs { get; set; }
        public List<BlogMedia> BlogMedias { get; set; }
    }
}
