﻿using System.Collections.Generic;

namespace HubertDumas.DAL.Media
{
    public interface IMediaRepository : IRepository<Media>
    {
    }
}