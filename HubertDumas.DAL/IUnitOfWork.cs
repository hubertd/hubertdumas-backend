﻿using HubertDumas.DAL.Blogs;
using HubertDumas.DAL.IdentityServer;
using HubertDumas.DAL.Media;
using HubertDumas.DAL.User;
using System;

namespace HubertDumas.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IClientRepository Clients { get;  }
        IUserRepository Users { get; }
        IMediaRepository Medias { get; }
        IBlogRepository Blogs { get; }
        IBlogMediaRepository BlogMedias { get; }
        int Save();
    }
}
