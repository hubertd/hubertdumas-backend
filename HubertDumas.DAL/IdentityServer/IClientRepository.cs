﻿namespace HubertDumas.DAL.IdentityServer
{
    public interface IClientRepository
    {
        Client GetClient(int id);
        Client GetClient(string clientId);
    }
}