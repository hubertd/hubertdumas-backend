﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HubertDumas.DAL.IdentityServer
{
    public class AuthorizationServer
    {
        public Guid Id { get; set; }
        public string Url { get; set; }

        public List<Client> Clients { get; set; }
    }
}
