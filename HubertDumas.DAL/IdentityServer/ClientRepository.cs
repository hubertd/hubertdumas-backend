﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HubertDumas.DAL.IdentityServer
{
    public class ClientRepository : Repository<Client>, IClientRepository
    {
        private readonly SweetSourContext _entities;

        public ClientRepository(SweetSourContext context) : base(context)
        {
            _entities = context;
        }

        public Client GetClient(int id)
        {
            return _entities.Clients.SingleOrDefault(x => x.Id == id);
        }

        public Client GetClient(string clientId)
        {
            return _entities.Clients.Include(c => c.AuthorizationServer).SingleOrDefault(x => x.ClientId == clientId);
        }
    }
}
