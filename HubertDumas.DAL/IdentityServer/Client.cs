﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HubertDumas.DAL.IdentityServer
{
    public class Client
    {
        public int Id { get; set; }
        public Guid AuthorizationServerId { get; set; }
        public bool Enabled { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientUri { get; set; }

        public AuthorizationServer AuthorizationServer { get; set; }
    }
}
