﻿using System.Collections.Generic;
using System.Linq;

namespace HubertDumas.DAL.User
{
    public class UserRepository : Repository<User>, IUserRepository
    {

        private readonly SweetSourContext _entities;

        public UserRepository(SweetSourContext context) : base(context)
        {
            _entities = context;
        }

        public User GetByCredentials(string username, string password)
        {
            return _entities.Users.SingleOrDefault(x => x.Username == username && x.Password == password);
        }
    }
}
