﻿using System.Collections.Generic;

namespace HubertDumas.DAL.Blogs
{
    public interface IBlogRepository : IRepository<Blog>
    {
    }
}