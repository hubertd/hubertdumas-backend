﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HubertDumas.DAL.Blogs
{
    public class BlogMediaRepository : Repository<BlogMedia>, IBlogMediaRepository
    {
        private readonly SweetSourContext _entities;

        public BlogMediaRepository(SweetSourContext context) : base(context)
        {
            _entities = context;
        }

        public List<BlogMedia> GetByBlog(int blogId)
        {
            return _entities.BlogMedias.Where(x => x.BlogId == blogId).ToList();
        }
    }
}
