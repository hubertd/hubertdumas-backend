﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
namespace HubertDumas.DAL.Blogs
{
    public class BlogRepository : Repository<Blog>, IBlogRepository
    {
        private readonly SweetSourContext _entities;

        public BlogRepository(SweetSourContext context) : base(context)
        {
            _entities = context;
        }
    }
}
