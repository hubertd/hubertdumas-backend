﻿using System.Collections.Generic;

namespace HubertDumas.DAL.Blogs
{
    public interface IBlogMediaRepository : IRepository<BlogMedia>
    {
        List<BlogMedia> GetByBlog(int blogId);
    }
}