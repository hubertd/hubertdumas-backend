﻿using System;
using System.Collections.Generic;

namespace HubertDumas.DAL.Blogs
{
    public class Blog
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //[Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string BlogText { get; set; }
        public int? MediaId { get; set; }

        public Media.Media Media { get; set; }
        public List<BlogMedia> BlogMedias { get; set; }
    }
}
