﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HubertDumas.DAL.Blogs
{
    public class BlogMedia
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public int BlogId { get; set; }
        public DateTime BlogUtcDate { get; set; }
        public int MediaId { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public string LongDescription { get; set; }

        public Blog Blog { get; set; }
        public HubertDumas.DAL.Media.Media Media { get; set; }
    }
}
