﻿//using HdCommon.Services;
//using HubertDumas.DAL;
//using IdentityServer.Services;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Microsoft.Extensions.DependencyInjection
//{
//    public static class HdIdentityServerBuilderExtensions
//    {
//        public static IIdentityServerBuilder AddCustomUserStore(this IIdentityServerBuilder builder)
//        {
//            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
//            builder.Services.AddScoped<ISecurityService, SecurityService>();
//            builder.AddProfileService<HdProfileService>();
//            builder.AddResourceOwnerValidator<HdResourceOwnerPasswordValidator>();

//            return builder;
//        }
//    }
//}
