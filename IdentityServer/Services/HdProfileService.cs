﻿using HdCommon.Services;
using HubertDumas.DAL.User;
using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public class HdProfileService : IProfileService
    {
        private ISecurityService _securityService;

        public HdProfileService(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();

            var user = _securityService.GetUserById(Convert.ToInt16(sub));
            var claims = new List<Claim>
            {
                new Claim("username", user.Username),
                new Claim("firstname", user.Firstname),
                new Claim("lastname", user.Lastname)
            };

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = _securityService.GetUserById(Convert.ToInt16(sub));
            context.IsActive = user != null;
        }
    }
}
