﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HdCommon.Models
{
    public class ClientConfiguration
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientUrl { get; set; }
        public Guid IdentityServerId { get; set; }
        public string IdentityServerUrl { get; set; }
    }
}
