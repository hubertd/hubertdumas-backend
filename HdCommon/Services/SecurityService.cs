﻿using HdCommon.Models;
using HubertDumas.DAL;
using HubertDumas.DAL.IdentityServer;
using HubertDumas.DAL.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace HdCommon.Services
{
    public class SecurityService : ISecurityService
    {
        private IUnitOfWork _unitOfWork;

        public SecurityService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public string Hash(string value)
        {
            StringBuilder Sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        public ClientConfiguration GetClientConfiguration(string clientId)
        {
            Client client = _unitOfWork.Clients.GetClient(clientId);

            if (client != null)
            {
                ClientConfiguration config = new ClientConfiguration
                {
                    ClientId = clientId,
                    ClientName = client.ClientName,
                    ClientUrl = client.ClientUri,
                    IdentityServerId = client.AuthorizationServerId,
                    IdentityServerUrl = client.AuthorizationServer.Url
                };

                return config;
            }

            return null;
        }

        public List<User> GetUsers(int qty = 0)
        {
            var users = _unitOfWork.Users.Get(qty).ToList();

            foreach(var user in users)
            {
                if (user.LastLogin.HasValue)
                {
                    user.LastLogin = DateTime.SpecifyKind((DateTime)user.LastLogin, DateTimeKind.Utc);
                }
            }

            return users;
        }

        public User GetUserByCredentials(string username, string password)
        {
            return _unitOfWork.Users.GetByCredentials(username, this.Hash(password));
        }

        public User GetUserById(int id)
        {
            return _unitOfWork.Users.GetById(id);
        }

        public User GetUserByUsername(string username)
        {
            return _unitOfWork.Users.Find(x => x.Username == username).FirstOrDefault();
        }

        public void AddUser(User user)
        {
            _unitOfWork.Users.Add(user);
            _unitOfWork.Save();
        }
        
        public void UpdateUser(User user)
        {
            _unitOfWork.Users.Update(user);
            _unitOfWork.Save();
        }
    }

}
