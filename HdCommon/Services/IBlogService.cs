﻿using System.Collections.Generic;
using HubertDumas.DAL.Blogs;

namespace HdCommon.Services
{
    public interface IBlogService
    {
        List<Blog> GetBlogs(int qty);
        Blog GetBlogById(int id);
        List<BlogMedia> GetBlogMediasByBlog(int blogId);
        void UpdateBlog(Blog blog);
        Blog AddBlog(Blog blog);
    }
}