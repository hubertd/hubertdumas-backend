﻿using HubertDumas.DAL.Media;
using HubertDumas.DAL;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace HdCommon.Services
{
    public class MediaService : IMediaService
    {
        private IUnitOfWork _unitOfWork;

        public MediaService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Media> GetMedias(int qty)
        {
            return _unitOfWork.Medias.Get(qty).ToList();
        }

        public Media GetById(int id)
        {
            return _unitOfWork.Medias.GetById(id);
        }

        public void AddMedia(Media media)
        {
            _unitOfWork.Medias.Add(media);
            _unitOfWork.Save();
        }
    }
}
