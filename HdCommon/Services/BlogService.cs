﻿using HubertDumas.DAL;
using HubertDumas.DAL.Blogs;
using System.Linq;
using System.Collections.Generic;

namespace HdCommon.Services
{
    public class BlogService : IBlogService
    {
        private IUnitOfWork _unitOfWork;

        public BlogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Blog> GetBlogs(int qty)
        {
            return _unitOfWork.Blogs.Get(qty).ToList();
        }

        public Blog GetBlogById(int id)
        {
            return _unitOfWork.Blogs.GetById(id);
        }

        public List<BlogMedia> GetBlogMediasByBlog(int blogId)
        {
            return _unitOfWork.BlogMedias.GetByBlog(blogId);
        }

        public void UpdateBlog(Blog blog)
        {
            _unitOfWork.Blogs.Update(blog);
            _unitOfWork.Save();
        }

        public Blog AddBlog(Blog blog)
        {
            _unitOfWork.Blogs.Add(blog);
            _unitOfWork.Save();
            return blog;
        }
    }
}
