﻿using HdCommon.Models;
using HubertDumas.DAL.User;
using System.Collections.Generic;

namespace HdCommon.Services
{
    public interface ISecurityService
    {
        string Hash(string value);
        ClientConfiguration GetClientConfiguration(string clientId);
        List<User> GetUsers(int qty = 0);
        User GetUserByCredentials(string username, string password);
        User GetUserById(int id);
        User GetUserByUsername(string username);
        void AddUser(User user);
        void UpdateUser(User user);
    }
}
