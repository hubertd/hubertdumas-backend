﻿using HubertDumas.DAL.Media;
using System.Collections.Generic;

namespace HdCommon.Services
{
    public interface IMediaService
    {
        List<Media> GetMedias(int qty);
        Media GetById(int id);
        void AddMedia(Media media);
    }
}