﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HdCommon.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HdApi.Controllers
{
    [Route("api/open-configuration")]
    [ApiController]
    public class OpenConfigurationController : ControllerBase
    {

        private ISecurityService _securityService;

        public OpenConfigurationController(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult GetClientConfiguration(string id)
        {
            return new JsonResult(_securityService.GetClientConfiguration(id));
        }

    }
}