﻿using System.Linq;
using HdCommon.Services;
using HubertDumas.DAL.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HdApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private ISecurityService _securityService;

        public IdentityController(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }

        [HttpGet("users")]
        public IActionResult GetUsers()
        {
            return new JsonResult(_securityService.GetUsers());
        }

        [HttpGet("user/{id:int}")]
        public IActionResult GetUser(int id)
        {
            return new JsonResult(_securityService.GetUserById(id));
        }

        [HttpPut("user")]
        public IActionResult AddUser(User user)
        {
            user.Password = _securityService.Hash(user.Password);
            _securityService.AddUser(user);
            return CreatedAtAction(nameof(GetUser), new { id = user.Id }, user);
        }

        [HttpPost("user")]
        public IActionResult UpdateUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return new StatusCodeResult(500);
            }

            _securityService.UpdateUser(user);
            return new JsonResult(user);
        }

        [HttpPost("userpassword/{newPassword}")]
        public IActionResult UpdateUserWithPassword(string newPassword, User user)
        {
            if (!ModelState.IsValid)
            {
                return new StatusCodeResult(500);
            }

            user.Password = _securityService.Hash(newPassword);

            _securityService.UpdateUser(user);
            return new JsonResult(user);
        }
    }
}