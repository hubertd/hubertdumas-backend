﻿using HdCommon.Services;
using HubertDumas.DAL.Blogs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HdApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class BlogController : ControllerBase
    {

        private IBlogService _blogService;

        public BlogController(IBlogService blogService)
        {
            _blogService = blogService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult(_blogService.GetBlogs(0));
        }

        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetBlog(int id)
        {
            return new JsonResult(_blogService.GetBlogById(id));
        }

        [HttpPost]
        public IActionResult SaveBlog(Blog blog)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(500, "Model is invalid");
            }

            try
            {
                if (blog.Id == 0)
                {
                    blog = _blogService.AddBlog(blog);
                    return CreatedAtAction(nameof(GetBlog), new { id = blog.Id }, blog);
                }
                else
                {
                    _blogService.UpdateBlog(blog);
                    return new JsonResult(blog);
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex);
            }
            
        }

        [HttpGet]
        [Route("blog-medias/{blogId:int}")]
        public IActionResult GetBlogMediasByBlog(int blogId)
        {
            return new JsonResult(_blogService.GetBlogMediasByBlog(blogId));
        }

    }
}