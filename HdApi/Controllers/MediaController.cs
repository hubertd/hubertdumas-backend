﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HdCommon.Services;
using HubertDumas.DAL.Media;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HdApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private IMediaService _mediaService;
        
        public MediaController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }

        [HttpGet]
        public IActionResult GetMedias()
        {
            return new JsonResult(_mediaService.GetMedias(0));
        }

        [HttpGet("{id:int}")]
        public IActionResult GetMedia(int id)
        {
            return new JsonResult(_mediaService.GetById(id));
        }

        [HttpPut]
        public IActionResult AddMedia(Media media)
        {
            _mediaService.AddMedia(media);
            return CreatedAtAction(nameof(GetMedia), new { id = media.Id }, media);
        }

        //[Route("upload")]
        //[HttpPost]
        //public async Task<IActionResult> Upload(List<IFormFile> files)
        //{
        //    long size = files.Sum(f => f.Length);
        //    var filePath = Path.GetTempFileName();

        //    foreach (var formFile in files)
        //    {
        //        if (formFile.Length > 0)
        //        {
        //            using (var stream = new FileStream(filePath, FileMode.Create))
        //            {
        //                await formFile.CopyToAsync(stream);
        //            }
        //        }
        //    }

        //    return Ok(new { count = files.Count, size, filePath });
        //}

        [Route("upload")]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Media");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    Media media = new Media
                    {
                        Type = 1,
                        Source = fileName
                    };

                    _mediaService.AddMedia(media);

                    //return Ok(new { dbPath });
                    return Ok(media);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}